from rest_framework import viewsets
from rest_framework.permissions import AllowAny

from account.models import User
from account.serializers import UserSerializer
from account.permissions import IsLoggedInUserOrAdmin, IsAdminUser

class UserViewSet(viewsets.ModelViewSet):
    # @method_decorator(csrf_exempt)
    # def dispatch(self, *args, **kwargs):
    #     return super(UserViewSet, self).dispatch(*args, **kwargs)

    queryset = User.objects.all()
    serializer_class = UserSerializer

    # def get_permissions(self):
    #     permission_classes = []
    #     if self.action == 'create':
    #         permission_classes = [AllowAny]
    #     elif self.action == 'retrieve' or self.action == 'update' or self.action == 'partial_update':
    #         permission_classes = [IsLoggedInUserOrAdmin]
    #     elif self.action == 'list' or self.action == 'destroy':
    #         permission_classes = [IsAdminUser]
    #     return [permission() for permission in permission_classes]

